
# Memorial

%#todo:write

## I - [Raison d'Être](https://en.wiktionary.org/wiki/raison_d%27%C3%AAtre)

GuayaHack, como fenómeno, es resultado del interés y la curiosidad de la comunidad de habla hispana en Internet. GuayaHack es, y será siempre, un espacio de aprendizaje colaborativo de y para la comunidad a fin de empoderar atraves de la educación y la experimentación a tod@s sus participantes.

## II - TTP (Tools, Techniques and Procedures)



## III - Identidad

La identidad de GuayaHack y sus miembros no se basa en características superficiales como apariencia, credo, orientación sexual, procedencia, estrato social, afiliación política y demás. 

Por el contrario, el único requisito para participar es el compromiso total con el empoderamiento de los demás, antes que el propio, a través de la educación al igual que el dominio y uso de tecnologías de relevancia e interés común.

## IV - Consideraciones Finales

`#todo:write`




