
```{post} 2023-07-18
:author: "@shavenbaboon"
:tags: participante
:category: miembros
:language: Español, Inglés
:location: Colombia
:excerpt: 1
```

# @shavenbaboon

Hola soy `@shavenbaboon`! 

Soy Electrónico pero me interesa el lado de sistemas. Crecí en Estados Unidos pero vivo en Colombia. 

Me interesan los idiomas, emprendimiento, filosofía y malabarismo. Me gusta la simplicidad, lo minimalista y las ideas. Para diversión me encanta la música, la comedia, twitch, youtube, reddit, etc.

Éste es mi espacio en GuayaHack.

## TODO

```console
$ git status 
On branch master
Your branch is up to date with 'origin/master'.

nothing to commit, working tree clean
```
