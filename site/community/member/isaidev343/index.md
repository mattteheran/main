# @isai.Dev343"

![Avatar](https://tecnofacil.s3.amazonaws.com/monta+puercos.jpg)

## ¡Hola a todos

¡Hola! Soy Isaí Hernandez un entusiasta de la tecnología y un apasionado por aprender y compartir conocimientos. Actualmente trabajo como desarrollador y analista de sistemas.

### Sobre mí

- **Ubicación:** Colombia 🇨🇴
- **Idioma:** Español 🇪🇸

### Mis habilidades

Me siento cómodo trabajando con diversas tecnologías, entre ellas:

- ReactJS
- Node.js
- Bases de Datos

### Intereses

Me encanta  el mundo de la ciencia ficción. Soy fan de los videojuegos a pesar de ya no tener tanto tiempo como antes :(

### Gustos musicales

De todo pero estoy mas inclinado por el rock , pop y rap.

### Contacto

- Email: <isaidj1999@gmail.com>
- GitHub: [@isaidj](https://github.com/isaidj)

¡Estoy emocionado de ser parte de esta comunidad y espero poder compartir conocimientos y aprender de todos ustedes! Si tienen alguna pregunta, no duden en contactarme.

¡Nos vemos pronto!
